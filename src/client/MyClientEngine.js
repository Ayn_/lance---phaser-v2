import ClientEngine from 'lance/ClientEngine';
import MyRenderer from './MyRenderer';
import KeyboardControls from 'lance/controls/KeyboardControls';

export default class BrawlerClientEngine extends ClientEngine {

    constructor(gameEngine, options) {
        super(gameEngine, options, MyRenderer);

        console.log('ClientEngine')

        // show try-again button
        gameEngine.on('objectDestroyed', (obj) => {
            if (obj.playerId === gameEngine.playerId) {
                console.log('You died?');
            }
        });

        this.controls = new KeyboardControls(this);
        this.controls.bindKey('up', 'up', { repeat: true } );
        this.controls.bindKey('down', 'down', { repeat: true } );
        this.controls.bindKey('left', 'left', { repeat: true } );
        this.controls.bindKey('right', 'right', { repeat: true } );
        this.controls.bindKey('space', 'space');

        // Once the phaser scene is ready - Start
        this.renderer.sceneReady.promise.then((scene)=>{

            // gameEngine.spaceHeight = scene.game.config.height
            // gameEngine.spaceWidth = scene.game.config.width

            this.connect(); 
        })
    }


}
