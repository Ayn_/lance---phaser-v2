import Phaser from 'phaser'
import mushroom from '../assets/images/mushroom.png'
import dude from '../assets/images/dude.png'

export default class extends Phaser.Scene {
  constructor () {
    super({ key: 'Boot' })
  }

  preload () {
    const progress = this.add.graphics()

    this.load.on('fileprogress', (file, value) => {
      progress.clear()
      progress.fillStyle(0xffffff, 0.75)
      progress.fillRect(700 - (value * 600), 250, value * 600, 100)
    })

    this.load.spritesheet('dude', dude, { frameWidth: 32, frameHeight: 48 })
    this.load.image('mushroom', mushroom)
  }

  create () {
    console.log('Bootscene ran')
  
    this.scene.start('Login')
  }
}
