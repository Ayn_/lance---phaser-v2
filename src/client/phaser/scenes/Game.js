import Phaser from 'phaser'
import { Mushroom, Dude } from '../sprites'

export default class extends Phaser.Scene {
  constructor () {
    super({ key: 'Game' })
  }

  create () {
    console.log('Game scene ran')

    this.tweens.add({
        targets: this.add.text(0, 0, 'Hello World', { fontFamily: '"Roboto Condensed"' }),
        x: 700,
        duration: 3000,
        ease: 'Power2',
        loop: -1
    });

    this.players = {}

    // Telling server to find a match
    //this.game.lance.clientEngine.socket.emit('scene_ready')
  }

  renderPlayer(obj) {
    return this.players[obj.id] = new Dude(this, obj.x, obj.x, 'dude', obj.id)
  }

  unrenderPlayer(obj) {
    this.players[obj.id].destroy();
  }

  update() {
    for (var player in this.players) {
      this.players[player].update()
    }
  }
}