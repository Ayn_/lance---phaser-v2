import Phaser from 'phaser'

export default class extends Phaser.Scene {
  constructor () {
    super({ key: 'Login' })
  }

  create () {
    console.log('login scene ran')

   //var element = this.add.dom(100, 200).createFromCache('login');
   
   const button = this.add.text(this.sys.game.config.width/2, this.sys.game.config.height/2, 'Find Match', { fill: '#FFF' });
   button.x = ((this.sys.game.config.width/2)-button.width/2)+((this.sys.game.config.width/2)-button.width/2)%2
   button.y = ((this.sys.game.config.height/2)-button.height/2)+((this.sys.game.config.height/2)-button.height/2)%2
   button.setInteractive();


    button.on('pointerdown', () => { 
      
      if(button.text == 'Find Match') {
        
        // Telling server to find a match
        this.game.lance.clientEngine.socket.emit('find_match')
        
        let time =  new Date()
            time.setMinutes(0)
            time.setSeconds(0)
        
        // Initial time
        button.text = 'Finding Match: 0:00'   
        button.x = ((this.sys.game.config.width/2)-button.width/2)+((this.sys.game.config.width/2)-button.width/2)%2
        button.y = ((this.sys.game.config.height/2)-button.height/2)+((this.sys.game.config.height/2)-button.height/2)%2

        var match = setInterval(()=>{
          time.setSeconds(time.getSeconds() + 1);
          button.text = 'Finding Match: ' + time.getMinutes() + ':' + ("0" + time.getSeconds()).slice(-2)
          button.x = ((this.sys.game.config.width/2)-button.width/2)+((this.sys.game.config.width/2)-button.width/2)%2
          button.y = ((this.sys.game.config.height/2)-button.height/2)+((this.sys.game.config.height/2)-button.height/2)%2
        }, 1000)

      } else if(button.text == 'Finding Match') { 
        button.text = 'Find Match'
      }

      // Once the is found
      this.game.lance.clientEngine.socket.on('found', (id) => {
        console.log('Match found!')
        clearInterval(match)
        this.scene.start('Game', id)
      })
      
    });

  }

  renderPlayer() {

  }
 
  update() {

  }
}
