export default class extends Phaser.GameObjects. Sprite {
    constructor(scene, x, y, sprite, name) {
        super(scene, x, y, sprite)

        this.id = name

        scene.add.existing(this)
        
        if(!scene.anims.get('left')) {
            scene.anims.create({
                key: 'left',
                frames: scene.anims.generateFrameNumbers('dude', { start: 0, end: 3 }),
                frameRate: 10,
                repeat: -1
            })

            scene.anims.create({
                key: 'turn',
                frames: [ { key: 'dude', frame: 4 } ],
                frameRate: 20
            })

            scene.anims.create({
                key: 'right',
                frames: scene.anims.generateFrameNumbers('dude', { start: 5, end: 8 }),
                frameRate: 10,
                repeat: -1
            })
        }

        this.anims.play('turn', true)

        // Use to track positional changes and change animations in the update loop.
        this.position = { x: this.x, y: this.y }

        this.name = scene.add.text(x, y, name, { fontSize: '20px', fill: '#fff' })
    
        this.on('destroy', ()=>this.name.destroy())
    }

    update () {
        this.name.setPosition(this.x-this.name.width/2, this.y-40)

        if(this.anims) {
            if(this.position.x > this.x) {
                this.position.x = this.x
                this.anims.play('left', true)
            } else if(this.position.x < this.x) {
                this.position.x = this.x
                this.anims.play('right', true)
            } else if (this.anims.currentAnim.key != 'turn') {
                this.anims.play('turn', true)
            }
        }

    }
}
