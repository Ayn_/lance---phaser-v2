import Renderer from 'lance/render/Renderer';
const Deferred = require('defer-promise');
import 'phaser';
import { Boot, Login, Game } from './phaser/scenes'
import Player from '../common/Player';
export default class MyRenderer extends Renderer {
    constructor(gameEngine, clientEngine) {
        super(gameEngine, clientEngine);
        this.game = gameEngine;
        this.sceneReady = Deferred();
        this.currentScene
        console.log('RenderEngine')
    }
    // initialize renderer.
    init() {
      
        // Initialize phaser
        return new Promise((resolve, reject) => {
            
            const config = {
                type: Phaser.AUTO,
                parent: 'game',
                width: 1024,
                height: 768,
                // scale: {
                //     mode: Phaser.Scale.RESIZE,
                //     autoCenter: Phaser.Scale.CENTER_BOTH
                // },
                scene: [
                    Boot,
                    Login,
                    Game
                ]
            }
                
            this.phaser = new Phaser.Game(config)
            this.phaser.lance = this
            console.log(this.phaser)
            resolve();
        });
    }
    // add a single player game object
    addPlayer(obj) {
        if(this.currentScene.renderPlayer) this.currentScene.renderPlayer(obj)
    }
    // remove player
    removePlayer(obj) {
        if(this.currentScene.unrenderPlayer) this.currentScene.unrenderPlayer(obj)  
    }
    // use to update phaser objects.
    draw(t, dt) {
        super.draw(t, dt);
        // Phaser scene is ready
        if(!this.phaser.scene.isBooted) return
        // Track scene
        if(!this.currentScene) {
            this.phaser.scene.scenes.forEach((scene)=>{
                scene.events.on('start', (scene)=>{
                    this.sceneReady.resolve(scene.scene.scene.key)
                    this.currentScene = this.phaser.scene.getScene(scene.scene.scene.key)
                })
            })
            return
        }
        // Update object positions
        this.game.world.forEachObject((id, obj) => {
            if (obj instanceof Player) {
                if(this.currentScene) {
                    this.phaser.scene.getScene(this.currentScene).children.list.forEach((item)=>{
                        // Auto update any sprite that has an id that matches a players.
                        if(item.type == 'Sprite' && item.id == id) {
                            item.x = obj.x
                            item.y = obj.y
                        }
                    })
                }
            }
        });
    }
}