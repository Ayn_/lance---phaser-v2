import GameEngine from 'lance/GameEngine';
import SimplePhysicsEngine from 'lance/physics/SimplePhysicsEngine';
import TwoVector from 'lance/serialize/TwoVector';
import Player from './Player';

export default class MyGameEngine extends GameEngine {

    constructor(options) {
        super(options);

        console.log('GameEngine')

        // game variables
        Object.assign(this, {
            spaceWidth: 160, spaceHeight: 90,
            fighterWidth: 10, fighterHeight: 12, jumpSpeed: 2,
            walkSpeed: 3,
        });

        this.physicsEngine = new SimplePhysicsEngine({
            //gravity: new TwoVector(0, 0),
            collisions: { type: 'bruteForce', autoResolve: true },
            gameEngine: this
        });

        this.inputsApplied = [];
        this.on('preStep', this.moveAll.bind(this));
    }

    registerClasses(serializer) {
        serializer.registerClass(Player);
    }

    processInput(inputData, playerId) {

        super.processInput(inputData, playerId);
        
        let player = this.world.queryObject({ playerId: playerId, instanceType: Player });

        if (player) {
            if (inputData.input === 'up') player.position.y -= this.walkSpeed;
            else if (inputData.input === 'down') player.position.y += this.walkSpeed;
            else if (inputData.input === 'right') player.position.x += this.walkSpeed;
            else if (inputData.input === 'left') player.position.x -= this.walkSpeed;
           //player.refreshFromPhysics(); // Causing wonky behaviour?
        }
    }

    // logic for every game step
    moveAll(stepInfo) {

        if (stepInfo.isReenact)
            return;

        // advance animation progress for all fighters
        let players = this.world.queryObjects({ instanceType: Player });
    }

    // create fighter
    addPlayer(playerId) {
        console.log('Player added to game (not rendered)')
        let f = new Player(this, null, { playerId, position: this.randomPosition() });
        f.height = this.fighterHeight;
        f.width = this.fighterWidth;
        f.direction = 1;
        f.progress = 0;
        f.action = 0;
        f.kills = 0;
        this.addObjectToWorld(f);
        return f;
    }

    // random position for new object
    randomPosition() {
        return new TwoVector(this.spaceWidth / 4 + Math.random() * this.spaceWidth/2, 70);
    }
}
