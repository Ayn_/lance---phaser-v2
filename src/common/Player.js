import BaseTypes from 'lance/serialize/BaseTypes';
import DynamicObject from 'lance/serialize/DynamicObject';
import Renderer from 'lance/render/Renderer';

export default class Player extends DynamicObject {

    // direction is 1 or -1
    // action is one of: idle, jump, fight, run, die
    // progress is used for the animation
    static get netScheme() {
        return Object.assign({
            direction: { type: BaseTypes.TYPES.INT8 },
            progress: { type: BaseTypes.TYPES.INT8 },
        }, super.netScheme);
    }

    onAddToWorld(gameEngine) {
        let renderer = Renderer.getInstance();
        if (renderer) renderer.addPlayer(this);
    }

    onRemoveFromWorld(gameEngine) {
        let renderer = Renderer.getInstance();
        if (renderer) renderer.removePlayer(this);
    }

    toString() {
        return `${Player}::${super.toString()} direction=${this.direction} progress=${this.progress}`;
    }

    syncTo(other) {
        super.syncTo(other);
        this.direction = other.direction;
        this.progress = other.progress;
    }
}
