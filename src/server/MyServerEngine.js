import ServerEngine from 'lance/ServerEngine';
import Player from '../common/Player';

let game = null;

export default class MyServerEngine extends ServerEngine {

    constructor(io, gameEngine, inputOptions) {
        super(io, gameEngine, inputOptions);
        console.log('ServerEngine')
        game = gameEngine;
        game.on('postStep', this.postStep.bind(this));
    }

    start() {
        super.start();
    
        if(!('match_making' in this.rooms)) this.createRoom('match_making')

        // // add floor
        // game.addPlatform({ x: 0, y: 0, width: 160 });

        // // add platforms
        // game.addPlatform({ x: 10, y: 25, width: 20 });
        // game.addPlatform({ x: 50, y: 35, width: 20 });
        // game.addPlatform({ x: 90, y: 35, width: 20 });
        // game.addPlatform({ x: 130, y: 25, width: 20 });

        // // add dinos
        // for (let i = 0; i < game.dinoCount; i++) {
        //     let f = game.addFighter(0);
        //     f.isDino = true;
        //     f.direction = 1;
        // }
    }

    // post-step state transitions
    postStep() {

        let player = game.world.queryObjects({ instanceType: Player });

        for (let f1 of player) {

            // check world bounds
            // f1.position.x = Math.max(f1.position.x, 0);
            // f1.position.x = Math.min(f1.position.x, game.spaceWidth - game.fighterWidth);

        }

        // reset input list
        game.inputsApplied = [];
    }

    onPlayerConnected(socket) {
        super.onPlayerConnected(socket);

        socket.on('find_match', () => {

            this.assignPlayerToRoom(socket.playerId, 'match_making')

            for(let player in this.connectedPlayers) {
                if(this.connectedPlayers[player].roomName == 'match_making') {
                    if(this.connectedPlayers[player].socket.playerId !== socket.playerId) {
                        console.log('Match found! ' + this.connectedPlayers[player].socket.playerId + ' with ' + socket.playerId)
                        
                        // Tell the players their match was found.
                        socket.emit('found')
                        this.connectedPlayers[player].socket.emit('found')

                        this.createRoom('/'+socket.playerId)
                        
                        // Asign both players to the room
                        this.assignPlayerToRoom(this.connectedPlayers[player].socket.playerId, '/'+socket.playerId)
                        this.assignPlayerToRoom(socket.playerId, '/'+socket.playerId)
                        
                        // Asign their bodies to the room
                        this.assignObjectToRoom(game.addPlayer(this.connectedPlayers[player].socket.playerId), '/'+socket.playerId)
                        this.assignObjectToRoom(game.addPlayer(socket.playerId), '/'+socket.playerId)

                        //console.log(this)
                    }
                }
            }
        })
    }

    onPlayerDisconnected(socketId, playerId) {
        super.onPlayerDisconnected(socketId, playerId);

        for (let o of game.world.queryObjects({ playerId })) {
            game.removeObjectFromWorld(o.id);
        }
    }
}